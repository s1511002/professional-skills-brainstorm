# Personal Academic Website

Potentially popular workshop, I've had several people ask for advice about
this. As opposed to other precedence for this kind of workshop, I think it'd
be good to be **very** open to the customizations that people want to make, as
opposed to walk through a specific template.


## Intro Talk
Topics:

Hosting options:
- School [website](https://www.maths.ed.ac.uk/~s1524448/)
    - free
    - quickest for existing static files (html, css)
    - NEED TO ASK ABOUT GLASGOW
- GitHub pages
    - free
    - quickest if you already like one of the templates
    - many examples/templates
        - [academicpages](https://github.com/academicpages/academicpages.github.io)
        - [Augustinas](https://github.com/ajacovskis/ajacovskis.github.io)
        - [Adam Howes](https://athowes.github.io/about)
        - [many here](https://github.com/topics/personal-website)
    - Some examples involve site generation (not just static files)
        - Not just HTML, CSS, JS
        - possibly cleaner to update when familiar
        - site generation taken care of by github actions
    - No server-side code (only one-time generation)
        - interactivity only possible if all logic in browser
            - esoteric [example](https://pseudowalls.gitlab.io/webapp/tilt.leptos/)
- Hosting services
    - typically monthly billing
    - upload static files
    - potential to run server-side code (likely irrelevant)
- WYSIWYG editors
    - quickest to create+host something from scratch
    - examples:
        - GAeL (https://sites.google.com/view/gaelxxx/home)
        - GEARS (https://sites.google.com/view/gears-seminar/2022-23)
    - options:
        - Squarespace
        - Wix
        - Weebly
        - Wordpress
        - Google Sites
        - etc.

Domain names:
- Longer term pricing (e.g. 10 GBP/year)
- should be able to use with any above options

## Workshop Section

For this topic in particular, it's more likely that attendants will arrive
with their own ideas in mind. It's worth asking ahead of time to potentially
help.

### Useful Expertise to Have on Standby:
- Patrick, Adrian, David (using school website)
- Sam (using google sites)
- Luke (github actions/pages)
