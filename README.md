# Professional Skills Brainstorm

## Workshop Ideas

- Tikz MasterClass
- [Personal Academic Website](personal-site.md)
- [Git](git.md)
- LaTeX installation tour
- [Terminals and Shells](unix-shells.md)
- Computer Algebra Systems
- Publishing Python/SageMath packages

## Overall Thoughts

### Proposed Format

Given the variety of topics, I think the goal for most sessions should not be
a tutorial as such but an exposition of what **can** be done, and an
opportunity to get **started** trying it.

#### Talk + Workshop
The format I have in mind for the topics above are a 15-30min presentation,
with the purpose of introducing the topic and demonstrating what can be done,
as opposed to proper instructions. Followed by 30-60min workshop time, to
either work through an (maybe a choice of) exercise(s) for some hands on
practice. Ideally even provide a space for people to bring their own projects
relating to the topic to work/talk about.

#### Expository Talk
If the above format doesn't make sense for a given topic, guest speaker, or
software setup. Then the odd session could involve an alternative seminar-like
format, where a speaker presents on a topic of interest to them, with the goal
of inspiring the use of tech in research.

### Devcontainers/Codespaces (a logistical solution)

To allow for independent topics each week, with software that people might
decide not to keep using, it would be nice to not have attendents install a
bunch of software each time (if possible), and have to deal with
troubleshooting each time.

Although this may not be possible for all topics, I believe that  every
session where it is set up, it will be worth it.

#### Precedence for Codespaces

Following the releases of [codespaces](https://github.com/features/codespaces)
from GitHub, the Computational Numerics, and Python Programming courses (by
Charlotte Desvages) have adopted it.
The main benefit was to set up the students of their course with a uniform
environment, which greatly improved how quickly they could get started with
the actual workshop content in the first few weeks, as well as restricting the
range of technical issues experienced.

#### Special Considerations for Our Case

##### Compute Limitations

The course mentioned above has some connections with GitHub which may not be
worth pursuing for a small workshop, however even free tier GitHub users enjoy
up to 60hrs of CodeSpaces compute time per month, which is hopefully much more
than necessary.

One other backup that comes for free when setting up for Codespaces, is that
any users with VSCode and Docker (see [corresponding section](VSCode +
Devcontainers extension)) can get an identical (superior) experience on their
own computer, modulo the time and space to download the associated container
(measured in GB). But has the benefits of avoiding software complications and
conflicts.

##### Variety of Software

The above mentioned courses had a single linux container image prepared to run
in the codespaces for the whole semester.
However this workshop will likely require different software each week,
creating a chore to prepare a new container image and setup for each session.
I (Luke) do have some experience with it at this point, in fact creating the
final container image used in the courses above to cut down in load times.
I have also been using devcontainers in my own (LaTeX) work, using
[existing container images](https://hub.docker.com/r/texlive/texlive)
when available, or
[creating my own when necessary](https://gitlab.com/lukenaylor/latex/sagetex-image/).

[DockerHub](https://hub.docker.com/) is a good place to look for existing
containers which could be useful. Two notable ones are:
- [TexLive](https://hub.docker.com/r/texlive/texlive) for LaTeX based
  workshops
- [Micromamba](https://hub.docker.com/r/mambaorg/micromamba) which can be
  used as a small base for any environment that can be created with
  [conda-forge](https://conda-forge.org/) packages (most OSS scientific computing).
  Here's a [reference guide](https://micromamba-docker.readthedocs.io/en/latest/quick_start.html)
  for building container images from it.

#### VSCode + Devcontainers extension

##### Linux Setup
A fairly large proportion of PhD students likely to attend chose to receive
Ubuntu laptops from the school. Luckily this is the easiest to install docker
on, requiring no deep virtualization to be enabled in the BIOS, just
`sudo apt install ...` a couple packages.
From then, the devcontainers extension needs to be installed in VSCode (note
not VSCodium), and at that point they're ready to get going.

##### Windows Setup
The most likely difficulty with the setup on Windows is running Docker, which
in turn requires WSL2. Microsoft has been pushing this tech quite hard since
2019 and the installation experience has gotten simpler though. One note is
that WSL2 requires a BIOS setting to be enabled, which may-or-may-not be
possible to enable without visiting the IT office. Anecdotally, the laptop
that I was given from the school in Sept 2020 had this option enabled and
there was no difficulty installing the above. The 2021 cohort did get
different laptops though.

##### MacOS Setup
I've done this once on an old MacBook air a while ago, I don't remember any
real hurdles.

##### OpenBSD Setup
I'm aware of a singular PhD student running none of the above. They're also
less likely to be keen to run Microsoft software. I'm sure they can manage
to do their own thing.
